IP2Location:
------------------------

Maintainers:
  Jeisson Perez Molano (http://drupal.org/user/143187)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
Module for integrate Web Service IP2Location (http://ip2location.com/web-service)

-------------
Go to "Admin" -> "Configuration" -> "ip2location" ->  to find
all the configuration options.
